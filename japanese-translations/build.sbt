import sbt.Keys._

import java.util.Calendar

lazy val root = (project in file(".")).
  settings(
    name := "japanese-translations",
    name in Universal := "japanese-translations",
    scalaVersion in ThisBuild := "2.11.8",
    scalacOptions ++= Seq(
      "-unchecked",
      "-deprecation",
      "-feature",
      "-language:implicitConversions",
      "-Xmax-classfile-name",
      "200"
    ),
    libraryDependencies ++= Seq(
      "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion,
      "org.scalatestplus.play" %% "scalatestplus-play" % scalaTestPlusPlayVersion % Test,
      "net.codingwell" %% "scala-guice" % scalaGuiceVersion,
      ws
    )
  ).
  enablePlugins(PlayScala).
  enablePlugins(BuildInfoPlugin).
  settings(
    buildInfoKeys := Seq[BuildInfoKey](
        name,
        version,
        BuildInfoKey.action("buildTime")(Calendar.getInstance().getTime),
        BuildInfoKey.action("commitHash")(sys.env.getOrElse("GIT_COMMIT","UNKNOWN"))),
    buildInfoPackage := "garvin.info"
  )

javaOptions in Test +="-Dlogger.resource=logback-test.xml"

lazy val scalaGuiceVersion = "4.0.1"
lazy val scalaLoggingVersion = "3.1.0"
lazy val scalaTestPlusPlayVersion = "1.5.0"
