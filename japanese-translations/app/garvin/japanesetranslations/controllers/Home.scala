package garvin.japanesetranslations.controllers

import com.typesafe.scalalogging.LazyLogging
import play.api.mvc.{Action, Controller}

import scala.concurrent.Future

class Home extends Controller with LazyLogging {

  def index = Action.async {
    Future.successful(
      Ok(garvin.japanesetranslations.views.html.home())
    )
  }

}
