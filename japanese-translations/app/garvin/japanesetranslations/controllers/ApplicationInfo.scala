package garvin.japanesetranslations.controllers

import garvin.info.BuildInfo

import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}

import java.net.InetAddress
import scala.concurrent.Future

class ApplicationInfo extends Controller with LazyLogging {

  def ping = Action {
    Ok("OK")
  }

  def info = Action {
    logger.info("Application Build Info -Project Name = " + BuildInfo.name)
    Ok(Json.toJson(Map("name" -> BuildInfo.name, "version" -> BuildInfo.version, "commitHash" -> BuildInfo.commitHash, "buildTime" -> BuildInfo.buildTime, "hostname" -> InetAddress.getLocalHost.getHostName)))
  }

  def health = Action.async { request =>
    Future.successful(Ok("OK"))
  }
}
