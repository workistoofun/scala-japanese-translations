package garvin.japanesetranslations.controllers

import garvin.japanesetranslations.services.DefinitionService

import com.google.inject.{ Inject, Singleton }
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import scala.concurrent.{ ExecutionContext, Future }

@Singleton
class Definitions @Inject()(
  val definitionService: DefinitionService
)(
  implicit val ec: ExecutionContext
) extends Controller with LazyLogging {

  def search(phrase: String, page: Int) = Action.async {
    logger.info(s"Looking up definition $phrase page $page")
    definitionService.getDefinitionByPage(phrase, page) map { json =>
      Ok(json)
    }
  }
}
