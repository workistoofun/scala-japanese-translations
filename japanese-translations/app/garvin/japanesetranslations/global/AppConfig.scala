package garvin.japanesetranslations.global

import com.google.inject.Singleton
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import garvin.info.BuildInfo

@Singleton
class AppConfig extends LazyLogging {

  val config = ConfigFactory.load

  val appEnv = config.getString("app.env")
  val applicationName = "Japanese Index Translation Microservice"

  val TranslationLookupServiceUrl = config.getString("translationapp.service.url")

  // as this is a singleton and used by classes that are eagerly loaded, this logging should occur early in startup
  logConfigItems()

  private def logConfigItems(): Unit = {
    logger.info(applicationName)
    logger.info(s"BuildInfo: Name=${BuildInfo.name}, Version=${BuildInfo.version}, BuildTime=${BuildInfo.buildTime}")
  }

}
