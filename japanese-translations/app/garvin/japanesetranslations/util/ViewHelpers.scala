package garvin.japanesetranslations.controllers.views

import controllers.routes
import play.api.Play
import play.api.Play.current
import play.twirl.api.Html

import java.io.File

object ViewHelpers {
  private implicit class AppendableHtml(a: Html) {
    def +=(b: Html) = Html(a.body + b.body)
  }

  def includeJavascripts: Html = {
    val public = Play.getFile("public/js")
    val filePaths = public.listFiles.map{ file =>
      "js/" + public.toPath.relativize(file.toPath).toString
    }
    filePaths
      .map(javascriptIncludeTag)
      .reduce(_ += _)
  }

  def includeStylesheets: Html = {
    val public = Play.getFile("public/css")
    val filePaths = public.listFiles.map{ file =>
      "css/" + public.toPath.relativize(file.toPath).toString
    }
    filePaths
      .map(stylesheetIncludeTag)
      .reduce(_ += _)
  }

  def stylesheetIncludeTag(stylesheetPath: String): Html = {
    Html(s"""\n<link rel="stylesheet" type="text/css" href="${routes.Assets.at(stylesheetPath)}">""")
  }

  def javascriptIncludeTag(scriptPath: String): Html = {
    Html(s"""\n<script type="text/javascript" src="${routes.Assets.at(scriptPath)}"></script>""")
  }

}
