package garvin.japanesetranslations.util

import play.api.Logger
import play.api.libs.ws.{ WSClient, WSRequest, WSResponse }
import scala.concurrent.{ ExecutionContext, Future }

object WSHelpers {

  class RetryableRequests(
    ws: WSClient,
    baseUrl: String,
    retries: Int = 2,
    retryDelayMs: Long = 1000
  )(implicit ec: ExecutionContext) {
    def getRequest(endpoint: String, retriesLeft: Int = retries): Future[WSResponse] = {
      ws.url(baseUrl + endpoint)
        .get()
        .recoverWith({
          case ex: Throwable =>
            if (retries > 0) {
              Logger.info(s"retrying request to $baseUrl")
              Thread.sleep(retryDelayMs)
              getRequest(baseUrl, retriesLeft - 1)
            }
            else {
              Future.failed(ex)
            }
        })
    }
  }
}
