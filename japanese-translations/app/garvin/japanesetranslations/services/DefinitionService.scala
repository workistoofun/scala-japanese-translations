package garvin.japanesetranslations.services

import garvin.japanesetranslations.global.AppConfig
import garvin.japanesetranslations.util.WSHelpers
import com.google.inject.{ ImplementedBy, Inject, Singleton }
import play.api.libs.ws.{ WSClient, WSRequest }
import scala.concurrent.{ ExecutionContext, Future }

@ImplementedBy(classOf[DefinitionServiceImpl])
trait DefinitionService {
  def getDefinitionByPage(phrase: String, page: Int): Future[String]
}

@Singleton
class DefinitionServiceImpl @Inject()(
  appConfig: AppConfig,
  ws: WSClient
)(
  implicit val ec: ExecutionContext
) extends DefinitionService {

  val wsRequests = new WSHelpers.RetryableRequests(
    ws,
    appConfig.TranslationLookupServiceUrl
  )

  def getDefinitionByPage(phrase: String, page: Int): Future[String] = {
    for {
      definitions <- wsRequests.getRequest(s"/search/$phrase/$page")
    } yield {
      definitions.body
    }
  }
}
