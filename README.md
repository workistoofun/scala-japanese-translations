* Japanese Index Translation App

This app provides a scala play abstraction of the original Golang version. Currently this app treats the original Golang service as a REST Api microservice for retrieving definitions

* Starting the app

    cd ./japanese-translations
    sbt run

* TODO

Dockerize this app and include the golang service together within the same docker-compose. As this is a scala code sample, I've only included the scala portion.